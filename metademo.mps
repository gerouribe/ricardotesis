%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -3 -3 3 3 
%%HiResBoundingBox: -2.5 -2.5 2.5 2.5 
%%Creator: MetaPost 1.803
%%CreationDate: 2013.11.26:1520
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor
newpath 2.5 0 moveto
2.5 0.663041 2.236608 1.298926 1.767767 1.767767 curveto
1.298926 2.236608 0.663041 2.5 0 2.5 curveto
-0.663041 2.5 -1.298926 2.236608 -1.767767 1.767767 curveto
-2.236608 1.298926 -2.5 0.663041 -2.5 0 curveto
-2.5 -0.663041 -2.236608 -1.298926 -1.767767 -1.767767 curveto
-1.298926 -2.236608 -0.663041 -2.5 0 -2.5 curveto
0.663041 -2.5 1.298926 -2.236608 1.767767 -1.767767 curveto
2.236608 -1.298926 2.5 -0.663041 2.5 0 curveto closepath fill
showpage
%%EOF
